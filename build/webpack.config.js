const base = require('./webpack.base.config')
const merge = require('webpack-merge')
// 让日志更加友好
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
// 查找开放端口或域接字的简单工具
const portfinder = require('portfinder')
const webpack = require('webpack')
const ora = require('ora')
const chalk = require('chalk')

let config, devConf
if (process.env.NODE_ENV === 'production') {
  config = require('./webpack.prod.config')

  const spinner = ora('building for production...')
  spinner.start()

  const compiler = webpack(merge(base, config))
  compiler.run((err, stats) => {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
  })
} else {
  config = require('./webpack.dev.config')
  devConf = merge(base, config)
  module.exports = new Promise((resolve, reject) => {
    portfinder.basePort = devConf.devServer.port
    portfinder.getPort((err, port) => {
      if (err) {
        reject(err)
      } else {
        // add port to devServer config
        devConf.devServer.port = port

        // Add FriendlyErrorsPlugin
        devConf.plugins.push(new FriendlyErrorsPlugin({
          compilationSuccessInfo: {
            messages: [`Your application is running here: http://${devConf.devServer.host}:${port}`]
          }
        }))

        resolve(devConf)
      }
    })
  })
}
