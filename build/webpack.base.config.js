const webpack = require('webpack')
const path = require('path')
const glob = require('glob')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// const CopyWebpackPlugin = require('copy-webpack-plugin')
// happypack
const HappyPack = require('happypack')
const os = require('os')
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length })
const createHappyPlugin = (id, loaders) => new HappyPack({
  id: id,
  loaders: loaders,
  threadPool: happyThreadPool,
  verbose: true
})

const src = path.resolve(__dirname, 'src')
const config = require('../config')

// 控制台信息
// const Dashboard = require('webpack-dashboard');
// const DashboardPlugin = require('webpack-dashboard/plugin');
// const dashboard = new Dashboard();

// 多页面打包
const getFilesName = (filesPath) => {
  let files = glob.sync(filesPath)
  let entries = []
  let entry, basename, extname

  for (let i = 0; i < files.length; i++) {
    entry = files[i]
    extname = path.extname(entry) // 扩展名 eg: .html
    basename = path.basename(entry, extname) // 文件名 eg: index
    entries.push(basename)
  }
  return entries
}
// const pages = getFilesName('src/pc/views/**.html') // html模版文件
const pages = getFilesName('src/pc/**.html') // html模版文件

const isDev = process.env.NODE_ENV === 'development'

const webpackConfig = {
  entry: config.entry,
  mode: process.env.NODE_ENV || 'development',
  resolve: {
    modules: ['node_modules'],
    alias: {
      '@': path.resolve('src/pc')
    }
  },
  output: {
    filename: 'js/[name].[hash].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: isDev ? '/' : config.publicPath
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      // chunks: 'async', // async表示只对异步代码进行分割
      minSize: 30000, // 当超过指定大小时做代码分割
      // maxSize: 200000,  // 当大于最大尺寸时对代码进行二次分割
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: '_',
      name: true,
      cacheGroups: { // 缓存组：如果满足vendor的条件，就按vender打包，否则按default打包
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10, // 权重越大，打包优先级越高
          // filename: 'js/vender.js'  //将代码打包成名为vender.js的文件
          name: 'vender'
        },
        default: {
          minChunks: 2,
          priority: -20,
          name: 'common',
          // filename: 'js/common.js',
          reuseExistingChunk: true // 是否复用已经打包过的代码
        }
      }
    }
  },
  plugins: [
    // copy custom static assets
    // new CopyWebpackPlugin([
    //   {
    //     from: path.resolve(__dirname, '../css'),
    //     to: path.resolve(__dirname, '../dist/css'),
    //     ignore: ['.*']
    //   }
    // ]),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash].css'
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    // PC端模版
    // new HtmlWebpackPlugin({
    //   filename: path.resolve(__dirname, '../dist/index.html'),
    //   template: 'src/pc/index.html',
    //   inject: true,
    //   minify: isDev ? false : {
    //     // 移除注释
    //     removeComments: true,
    //     // 不要留下任何空格
    //     collapseWhitespace: true,
    //     // 当值匹配默认值时删除属性
    //     removeRedundantAttributes: true,
    //     // 使用短的doctype替代doctype
    //     useShortDoctype: true,
    //     // 移除空属性
    //     removeEmptyAttributes: true,
    //     // 从style和link标签中删除type="text/css"
    //     removeStyleLinkTypeAttributes: true,
    //     // 保留单例元素的末尾斜杠。
    //     keepClosingSlash: true,
    //     // 在脚本元素和事件属性中缩小JavaScript(使用UglifyJS)
    //     minifyJS: true,
    //     // 缩小CSS样式元素和样式属性
    //     minifyCSS: true,
    //     // 在各种属性中缩小url
    //     minifyURLs: true,
    //     // 尽可能删除属性周围的引号
    //     removeAttributeQuotes: false
    //   },
    //   chunksSortMode: 'dependency',
    //   chunks: ['vender', 'app']
    // }),
    createHappyPlugin('happy-js', [{
      loader: 'babel-loader',
      options: {
        babelrc: true,
        cacheDirectory: true
      }
    }]),
    createHappyPlugin('happy-style', [
      'css-loader',
      {
        loader: 'postcss-loader',
        options: {
          ident: 'postcss',
          config: {
            path: path.resolve(__dirname, '../config/postcss.config.js')
          }
        }
      },
      'sass-loader'
    ])
    // createHappyPlugin('happy-style-mobile', [
    //   'css-loader',
    //   {
    //     loader: 'postcss-loader',
    //     options: {
    //       ident: 'postcss',
    //       config: {
    //         path: path.resolve(__dirname, '../postcss.config.js')
    //       }
    //     }
    //   },
    //   'sass-loader'
    // ])
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        // include: [path.resolve(__dirname, '../src/pc')],
        use: [
          isDev ? 'style-loader' : {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: true,
              reloadAll: true,
              publicPath: '../'
            }
          },
          'happypack/loader?id=happy-style'
        ]
      },
      // {
      //   test: /\.(css|scss|sass)$/,
      //   include: [path.resolve(__dirname, '../src/mobile')],
      //   use: [
      //     isDev ? 'style-loader' : {
      //       loader: MiniCssExtractPlugin.loader,
      //       options: {
      //         hmr: true,
      //         reloadAll: true
      //       }
      //     },
      //     'happypack/loader?id=happy-style-mobile'
      //   ]
      // },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        include: src,
        use: 'happypack/loader?id=happy-js'
      },
      {
        test: /\.js$/,
        use: {
          loader: 'eslint-loader',
          options: {
            formatter: require('eslint-friendly-formatter') // 默认的错误提示方式
          }
        },
        enforce: 'pre', // 编译前检查
        exclude: /node_modules/, // 不检测的文件
        include: path.resolve(__dirname, '../src') // 要检查的目录
      },
      // 处理图片
      {
        test: /\.(gif|png|ico|jpg|svg)\??.*$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 100,
            esModule: false,
            name: 'assets/images/[name].[hash].[ext]'
          }
        }]
      },
      // 处理多媒体文件
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)\??.*$/,
        loader: 'url-loader',
        options: {
          limit: 100,
          esModule: false,
          name: 'assets/media/[name].[hash].[ext]'
        }
      },
      // 处理字体文件
      {
        test: /\.(woff|woff2|eot|ttf|otf)\??.*$/,
        loader: 'url-loader',
        options: {
          limit: 100,
          esModule: false,
          name: 'assets/fonts/[name].[hash].[ext]'
        }
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: ['img:src', 'img:data-src', 'audio:src', 'link:href'],
            minimize: false
          }
        }
      }
    ]
  }
}

pages.forEach(function (fileName) {
  let setting = {
    // filename: 'views/' + fileName + '.html', // 生成的html存放路径，相对于path
    // template: 'src/pc/views/' + fileName + '.html', // html模板路径
    filename: fileName + '.html', // 生成的html存放路径，相对于path
    template: 'src/pc/' + fileName + '.html', // html模板路径
    inject: true // js插入的位置，true/'head'/'body'/false
  }

  // setting.favicon = './src/assets/img/favicon.ico'
  setting.chunks = ['vender', 'app']
  setting.chunksSortMode = 'dependency'
  setting.inject = 'body'
  setting.hash = true
  setting.minify = false
  webpackConfig.plugins.push(new HtmlWebpackPlugin(setting))
})

module.exports = webpackConfig
