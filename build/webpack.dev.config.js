// const HtmlWebpackPlugin = require('html-webpack-plugin')
// const path = require('path')
const webpack = require('webpack')

const config = require('../config')

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    hot: true,
    host: 'localhost',
    port: 8080,
    open: true,
    compress: true,
    publicPath: '/',
    contentBase: config.publicPath,
    openPage: 'index.html', // 指定默认启动浏览器时打开的页面
    index: 'index.html', // 指定首页位置
    // hotOnly: true, // HMR 不自动刷新
    inline: true, // 页面刷新
    noInfo: true, // webpack-dashboard
    stats: 'errors-only',
    // notifyOnErrors: true, // 跨平台错误提示
    overlay: true, // 在浏览器中显示错误
    before: function (app, server) {
      server._watch(`src/*/**.html`)
    }
  },
  plugins: [
    // PC端
    // new HtmlWebpackPlugin({
    //   filename: path.resolve(__dirname, '../dist/index.html'), // 最后生成的文件名
    //   template: 'src/pc/index.html', // 模版html
    //   chunks: ['vender', 'app'], // 注入打包后的js文件
    //   chunksSortMode: 'dependency',
    //   inject: true,
    //   favicon: path.resolve(__dirname, '../src/pc/assets/images/yinji.ico')
    // }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
    // 移动端
    // new HtmlWebpackPlugin({
    //   filename: path.resolve(__dirname, '../dist/mobile_index.html'),
    //   template: 'src/mobile/index.html',
    //   chunksSortMode: 'dependency',
    //   chunks: ['vender', 'mobile'],
    //   inject: true
    // })
  ]
}
