// const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
// const PurifyCSS = require('purifycss-webpack')
// const glob = require('glob-all')
const AddAssetHtmlWebpackPlugin = require('add-asset-html-webpack-plugin')
const webpack = require('webpack')
// gzip
// const CompressionPlugin = require('compression-webpack-plugin') //gzip

module.exports = {
  // devtool: 'cheap-module-source-map',
  stats: {
    // copied from `'minimal'`
    all: false,
    modules: true,
    maxModules: 0,
    errors: true,
    warnings: true,
    // our additional options
    moduleTrace: true,
    errorDetails: true
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: 4
      }),
      new OptimizeCssAssetsPlugin({})
    ],
    usedExports: true
  },
  plugins: [
    // 清除无用 css
    // new PurifyCSS({
    //   paths: glob.sync([
    //     // 要做 CSS Tree Shaking 的路径文件
    //     path.resolve(__dirname, '../src/**/*.html'),
    //     path.resolve(__dirname, './src/**/*.js')
    //   ])
    // }),
    // PC端模版
    // new HtmlWebpackPlugin({
    //   filename: path.resolve(__dirname, '../dist/index.html'),
    //   template: 'src/pc/index.html',
    //   inject: true,
    //   minify: {
    //     // 移除注释
    //     removeComments: true,
    //     // 不要留下任何空格
    //     collapseWhitespace: true,
    //     // 当值匹配默认值时删除属性
    //     removeRedundantAttributes: true,
    //     // 使用短的doctype替代doctype
    //     useShortDoctype: true,
    //     // 移除空属性
    //     removeEmptyAttributes: true,
    //     // 从style和link标签中删除type="text/css"
    //     removeStyleLinkTypeAttributes: true,
    //     // 保留单例元素的末尾斜杠。
    //     keepClosingSlash: true,
    //     // 在脚本元素和事件属性中缩小JavaScript(使用UglifyJS)
    //     minifyJS: true,
    //     // 缩小CSS样式元素和样式属性
    //     minifyCSS: true,
    //     // 在各种属性中缩小url
    //     minifyURLs: true,
    //     // 尽可能删除属性周围的引号
    //     removeAttributeQuotes: false
    //   },
    //   chunksSortMode: 'dependency',
    //   chunks: ['vender', 'app']
    // }),
    // 移动端模版
    // new HtmlWebpackPlugin({
    //   filename: path.resolve(__dirname, '../dist/mobile_index.html'),
    //   template: 'src/mobile/index.html',
    //   inject: true,
    //   minify: {
    //     // 移除注释
    //     removeComments: true,
    //     // 不要留下任何空格
    //     collapseWhitespace: true,
    //     // 当值匹配默认值时删除属性
    //     removeRedundantAttributes: true,
    //     // 使用短的doctype替代doctype
    //     useShortDoctype: true,
    //     // 移除空属性
    //     removeEmptyAttributes: true,
    //     // 从style和link标签中删除type="text/css"
    //     removeStyleLinkTypeAttributes: true,
    //     // 保留单例元素的末尾斜杠。
    //     keepClosingSlash: true,
    //     // 在脚本元素和事件属性中缩小JavaScript(使用UglifyJS)
    //     minifyJS: true,
    //     // 缩小CSS样式元素和样式属性
    //     minifyCSS: true,
    //     // 在各种属性中缩小url
    //     minifyURLs: true
    //   },
    //   chunksSortMode: 'dependency',
    //   chunks: ['vender', 'mobile']
    // }),
    // new CompressionPlugin({
    //   test: /\.js(\?.*)?$/i
    // }),
    new AddAssetHtmlWebpackPlugin({
      filepath: path.resolve(__dirname, '../dll/jquery.dll.js'), // 对应的 dll 文件路径
      outputPath: 'dll',
      publicPath: 'dll',
      includeSourcemap: false
    }),
    new webpack.DllReferencePlugin({
      manifest: path.resolve(__dirname, '..', 'dll/jquery-manifest.json')
    })
  ]
}
