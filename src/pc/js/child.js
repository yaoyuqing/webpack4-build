// wow.js
import { WOW } from 'wowjs'
import 'animate.css'

$(function () {
  header()
  slider()
  block()
  qrcode()
  wow()
})

function header () {
  if ($(document).scrollTop() > 0) {
    $('#header').addClass('mini')
    $('#shares .goTop').addClass('show')
  }
  $(document).scroll(function () {
    if ($(document).scrollTop() > 0) {
      $('#header').addClass('mini')
      $('#shares .goTop').addClass('show')
    } else {
      $('#header').removeClass('mini')
      $('#shares .goTop').removeClass('show')
    }
  })

  // share button
  $('#shares .goTop').on('click', function () {
    $('html,body').animate({ 'scrollTop': 0 }, 300)
    return false
  })
}

function qrcode () {
  $('#fixed').on('click', function (event) {
    if (event.target === this) {
      $(this).removeClass('show')
      $('#fixed .fixed-container').removeClass('show')
    }
  })
  $('#shares .mobile').click(function () {
    $('#fixed').addClass('show')
    $('#fixed .mb_qrcode').addClass('show')
  })
  $('#shares .weixin,#contact .wx').click(function () {
    $('#fixed').addClass('show')
    $('#fixed .wx_qrcode').addClass('show')
  })
}

function slider () {
  var $slider = $('#mslider .content_list')

  $slider.slick({
    dots: false,
    speed: 800,
    // autoplay: true,
    infinite: true,
    prevArrow: `<div class="slick-prev"><i class="iconfont icon-jiantou6"></i></div>`,
    nextArrow: `<div class="slick-next"><i class="iconfont icon-jiantou7"></i></div>`
  })
}

function block () {
  $('#block li').each(function () {
    $(this).on('mouseover', function () {
      $('#block li').removeClass('active')
      $(this).addClass('active')
    })
  })
}

function wow () {
  // project
  $('#project .content_list .item_block .item_img').addClass('wow zoomBig')
  $('#project .content_list .item_block .item_img').each(function (i, n) {
    $(n).attr('data-wow-delay', `${((i + 1) * 0.2).toFixed(1)}s`)
  })

  // service
  $('#service .content_list .item_block').addClass('wow fadeInUp')
  $('#service .content_list .item_block').each(function (i, n) {
    $(n).attr('data-wow-delay', `${((i + 1) * 0.2).toFixed(1)}s`)
  })

  // news
  $('#news .content_list .item_block').addClass('wow fadeInUp')
  $('#news .content_list .item_block').each(function (i, n) {
    $(n).attr('data-wow-delay', `${((i + 1) * 0.2).toFixed(1)}s`)
  })

  // honour
  $('#honour .content_list .item_block').addClass('wow fadeInUp')
  $('#honour .content_list .item_block').each(function (i, n) {
    $(n).attr('data-wow-delay', `${((i + 1) * 0.2).toFixed(1)}s`)
  })

  new WOW({ live: false }).init()
}
