import './assets/css/main.scss'
import Child from './child'

$('.container').css({
  background: '#cfcfcf'
})

var arr = [1, 2, 3, 4, 5, 6]

var fn = () => {
  console.log(arr.includes(1))
}
fn()

$('.wrapper').addClass('owl-carousel').owlCarousel()

$('.container').prepend(`<p>${Child}</p>`)

console.log('移动端', Child)

// if (module.hot) { // HMR
//     module.hot.accept('./child')
// }
